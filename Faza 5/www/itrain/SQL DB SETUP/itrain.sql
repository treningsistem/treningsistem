-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2015 at 04:27 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12


USE itrain;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`KID`, `Username`, `Password`, `Email`, `Blocked`, `TKID`) VALUES
(2, 'K1', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'k1@mail.com', 0, 1),
(3, 'K2', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'k2@mail.com', 0, 1),
(4, 'K3', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'k3@mail.com', 1, 1),
(5, 'K4', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'k4@mail.com', 0, 1),
(6, 'K5', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'k5@mail.com', 0, 1),
(7, 'K6', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'k6@mail.com', 0, 1),
(8, 'K7', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'k7@mail.com', 0, 2),
(9, 'M1', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'm1@mail.com', 0, 3),
(10, 'M2', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'm2@mail.com', 1, 3),
(11, 'T1', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 't1@mail.com', 0, 2),
(13, 'T2', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 't2@mail.com', 0, 2),
(14, 'T3', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 't3@mail.com', 0, 2),
(15, 'latinični', '$2y$10$oCEWkG06Xg6BKNiQJFGY1.ecVy9zvJ/xgno/5pA1sbuuHNOV5Mdm6', 'lat@mail.com', 0, 1),
(16, 'K8', '$2y$10$8r9AzAup9ZgPDeVAXWmU.ep9TQFnkK0tX5tk5poV1Qxx3kQtY26jC', 'k8@mail.com', 0, 1);

-- --------------------------------------------------------

--
-- Dumping data for table `treninzi`
--

INSERT INTO `treninzi` (`TID`, `Title`, `Description`, `Created`, `TTID`, `KID`) VALUES
(1, 'Тест тренинг', 'Ћирилични опис тест тренинга', '2015-05-25 13:37:40', 2, 3),
(2, 'Naslov', 'Javni trening 1', '2015-05-25 13:43:45', 1, 11),
(3, 'Trening bez blokova', 'Imaće samo opis', '2015-05-25 13:45:55', 2, 11),
(4, 'Javni trening bez blokova', 'Ovo je javni trening ali nema blokove', '2015-05-25 13:46:27', 1, 11),
(5, 'DEFAUL TITLE', 'DEFAULT DESCRIPTION', '2015-05-25 13:47:04', 2, 11),
(6, 'タイトルトレーニング', 'これは、日本のテキストです。', '2015-05-25 13:47:34', 1, 13),
(7, 'Korisnicki trening', 'Imace 10 blokova', '2015-05-25 13:50:01', 2, 6),
(8, 'Ovo je korisnik koji je unapre', 'Posto je unapredjen u trenera, sada moze da pravi javne treninge', '2015-05-25 14:09:15', 1, 8),
(9, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:10:34', 2, 5),
(10, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:10:44', 2, 5),
(11, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:10:51', 2, 5),
(12, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:11:02', 2, 5),
(13, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:11:11', 2, 5),
(14, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:11:11', 2, 5),
(15, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:11:11', 2, 5),
(16, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:11:11', 2, 5),
(17, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:11:11', 2, 5),
(18, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:11:11', 2, 5),
(19, 'K4 je spamer', 'K4 je spamer', '2015-05-25 14:11:11', 2, 5),
(20, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(39, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(40, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(41, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(42, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(43, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(44, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(45, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(46, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(47, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(48, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(49, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(50, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(51, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(52, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(53, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(54, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14),
(55, 'Javni trening', 'TEST PAGINACIJE', '2015-05-25 13:12:53', 1, 14);


-- --------------------------------------------------------

--
-- Dumping data for table `blokovi`
--

INSERT INTO `blokovi` (`BID`, `Title`, `Description`, `BlockNum`, `Duration`, `TID`) VALUES
(1, 'Блок', 'Блок са ћириличним описом', 0, 10, 1),
(2, 'Blok 1', 'Srpska latinica ČĆŠĐĆŽ', 0, 0, 2),
(3, 'Blok 2', '(｡◕ ∀ ◕｡) ｀ｨ(´∀｀∩ __ﾛ(,_,*)', 1, 2, 2),
(4, 'Блок 3', 'Ћирилица', 2, 12, 2),
(5, '日本のブロック', '日本語圏の長い説明、いくつかのシンボルと奇跡。', 0, 20, 6),
(6, '1', '10', 0, 5, 7),
(7, '2', '9', 1, 10, 7),
(8, '3', '8', 2, 15, 7),
(9, '4', '7', 3, 6, 7),
(10, '5', '6', 4, 13, 7),
(11, '6', '4', 5, 1, 7),
(12, '7', '4', 6, 2, 7),
(13, '8', '3', 7, 20, 7),
(14, '9', '2', 8, 20, 7),
(15, '10', '1', 9, 5, 7),
(16, 'I blokove', 'koje su deo javnih treninga', 0, 60, 8);