<?php
/* Autor: Stefan Rankovic 2014/3155
 * Testira da li se bilo gde u pregledu osoblja pojavljuje opcija za blokiranje i brisanje administratora
 */
 
class AdminBlock extends PHPUnit_Extensions_Selenium2TestCase
{
	protected function setUp()
    {
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://localhost/itrain');
    }

    public function testForme()
    {
		$username = 'admin';
        $password = 'admin';
 
        $this->url("http://localhost/itrain/index.php/start/staff");
        $usernameInput = $this->byName("username"); // unesemo username u polje za username
        $usernameInput->clear();
        $this->keys($username);
 
        $usernameInput = $this->byName("password"); // unesemo password u polje za password
        $usernameInput->clear();
        $this->keys($password);
		
		$this->byName('login_korisnik')->submit(); // submitujemo
		
		$osoblje_URL = "http://localhost/itrain/index.php/admin/osoblje";
		$this->url($osoblje_URL); // predjemo na URL za manipulaciju osobljem
		$this->assertTrue(strcmp($this->url(), $osoblje_URL) == 0, "Nismo na stranici za osoblje."); // proverimo da li je prelazak uspeo
		
		try 
		{
			$this->byXPath("//td[text()='Administrator']/../td/a"); // ako nadje link za brisanje administratora, failuje
			$this->fail("Postoji link za brisanje administratora"); // ako prethodna linija ne baci izuzetak, test se failuje
		} catch (Exception $e) { } // ako ne nadje element, sve je ok
    }

}
?>