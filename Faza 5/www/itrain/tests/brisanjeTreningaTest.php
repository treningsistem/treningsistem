<?php
//<!-- autor: Milica Stanković 2009/0459 -->

class BrisanjeTreninga extends PHPUnit_Framework_TestCase
{
    private $CI;

    public function setUp()
    {
        $this->CI =& get_instance();
        $this->CI->load->database('itrain');
        $this->CI->load->model('models/useractions');
    }

    public function testBrisiTrening() // testira da li može da se obrise trening
    {
        //kreiramo trening koji ce sluziti za brisanje
        $query = "INSERT INTO Treninzi (Title, Description, Created, TTID, KID) VALUES ('TEST', 'TEST', 0, 1, 3);";

        $this->CI->db->query($query); // ubacimo trening
        $id = $this->CI->db->insert_id(); // dohvatimo ID

        $query = "DELETE FROM Treninzi WHERE TID = ?;";
        $this->CI->db->query($query, array($id)); // obrisemo test trening - povratak u prethodno stanje

        $query = "SELECT TID FROM Treninzi WHERE TID = ?;";
        $res = $this->CI->db->query($query, array($id)); // probamo da dohvatimo kreirani trening
        if ($res->num_rows() > 0) {
            $this->fail("Nije uspelo brisanje treninga iz baze"); // failujemo test
        }


    }

}
?>