<?php
//<!-- autor: Milica Stanković 2009/0459 -->

class KreiranjeTreninga extends PHPUnit_Framework_TestCase
{
    private $CI;

    public function setUp()
    {
        $this->CI = &get_instance();
        $this->CI->load->database('itrain');
        $this->CI->load->model('models/useractions');
    }

    public function testNoviTrening() // testira da li može da se kreira novi trening
    {
        $query = "INSERT INTO Treninzi (Title, Description, Created, TTID, KID) VALUES ('TEST', 'TEST', 0, 1, 3);"; // dodamo novi

        $this->CI->db->query($query); // ubacimo test trening
        $id = $this->CI->db->insert_id(); // dohvatimo ID

        $query = "SELECT TID FROM Treninzi WHERE TID = ?;";
        $res = $this->CI->db->query($query, array($id)); // dohvatimo novi trening
        if ($res->num_rows() <= 0) {
            $this->fail("Nije uspeo upis novog treninga u bazu"); // failujemo test
        }

        $query = "DELETE FROM Treninzi WHERE TID = ?;";
        $this->CI->db->query($query, array($id)); // obrisemo test trening - povratak u prethodno stanje

    }

}
?>