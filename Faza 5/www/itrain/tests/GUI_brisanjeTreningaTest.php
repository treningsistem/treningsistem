<?php
/* Autor: Milica Stankovic 2009/0459
 * Testira da li korisnik moze da napravi obrise trening preko forme
 */
 
class GUIBrisanjeTreninga extends PHPUnit_Extensions_Selenium2TestCase
{
	private $CI;
	
    protected function setUp()
    {
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://localhost/itrain');
		$this->CI = &get_instance();
		$this->CI->load->database('itrain');
    }

    public function testForme()
    {
	
        // PRAVLJENJE TRENINGA ZA BRISANJE

        $query = "INSERT INTO Treninzi (Title, Description, Created, TTID, KID) VALUES ('TEST♥♦♣♠', 'TEST♥♦♣♠♥♦♣♠', 0, 1, 3);";

        $this->CI->db->query($query); // ubacimo trening
        $id = $this->CI->db->insert_id(); // dohvatimo ID
		
		// LOGIN KORISNIKA
        $username = 'K2';
        $password = 'sifra';
 
        $this->url("http://localhost/itrain/");
        $usernameInput = $this->byName("username"); // unesemo username u polje za username
        $usernameInput->clear();
        $this->keys($username);
 
        $usernameInput = $this->byName("password"); // unesemo password u polje za password
        $usernameInput->clear();
        $this->keys($password);
		
		$this->byName('login_korisnik')->submit(); // submitujemo

        // FORMA ZA PREGLED TRENINGA
		$index_URL = "http://localhost/itrain/index.php/korisnik/moji";
		$this->url($index_URL); // predjemo na stranicu za pregled korisnikovih treninga

        //brisanje upravo tog treninga preko forme

        $index_URL = "http://localhost/itrain/index.php/korisnik/obrisi/" . $id;
        $this->url($index_URL); // poziv funkcije: korisnik/obrisi/id
        //TODO: da li može da uzme GUI element "X" ikako?

		$index_URL = "http://localhost/itrain/index.php/korisnik/pregledaj/" . $id;
		$this->url($index_URL); // probamo da pregledamo trening
		$this->assertFalse(strcmp($this->url(), $index_URL) == 0, "Na stranici smo za pregled treninga"); // ako dospemo na pregled treninga, fail
		
		// UNDO PROMENA U BAZI

        $query = "DELETE FROM Treninzi WHERE Description = 'TEST♥♦♣♠♥♦♣♠';";
        $this->CI->db->query($query, array($id)); // obrisemo test trening - povratak u prethodno stanje

    }

}
?>