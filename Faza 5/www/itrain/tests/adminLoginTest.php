<?php
/* Autor: Stefan Rankovic 2014/3155
 * Testira da li administrator uspesno moze da se uloguje (delimicno vezano za SSU 1).
 */
 
class AdminTest extends PHPUnit_Framework_TestCase
{
	private $CI;
 
    public function setUp()
    {
        $this->CI = &get_instance();
        $this->CI->load->database('itrain');
		$this->CI->load->model('loginmodel');
    }
	
    public function testDobraSifra() // testira da li administrator moze da se uloguje sa ispravnim podacima
    {
		$res = $this->CI->loginmodel->loginSupervisor('admin', 'admin');
		$this->assertEquals($res['status'], $this->CI->loginmodel->LoginSuccessful(), "Administrator sa ispravnim username i password nije uspeo da se uloguje.");
	}
	
	public function testLosaSifra() // testira da li ce sistem odbiti administratorski nalog sa pogresnom sifrom
    {
		$res = $this->CI->loginmodel->loginSupervisor('admin', 'pogresnasifra');
		$this->assertEquals($res['status'], $this->CI->loginmodel->WrongUsernameOrPassword(), "Administrator sa neispravnom sifrom uspeo da se uloguje.");
	}
	
	public function testLosUsername() // testira da li ce sistem odbiti administratorski nalog sa nepostojecim username
    {
		$res = $this->CI->loginmodel->loginSupervisor('adminko', 'admin');
		$this->assertEquals($res['status'], $this->CI->loginmodel->WrongUsernameOrPassword(), "Administrator sa nepostojecim username uspeo da se uloguje.");
	}
}
?>