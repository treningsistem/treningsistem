<?php
/* Autor: Stefan Rankovic 2014/3155
 * Testira SSU 7 - Kreiranje naloga trenera.
 */
 
class UpgradeTrenera extends PHPUnit_Framework_TestCase
{
	private $CI;
 
    public function setUp()
    {
        $this->CI = &get_instance();
        $this->CI->load->database('itrain');
		$this->CI->load->model('models/useractions');
    }
	
    public function testUpgrade() // testira da li radi funkcionalnost upgrade korisnika
    {
		$query = "INSERT INTO Korisnici (Username, Password, Email, Blocked, TKID) VALUES ('TEST', 'TEST', 'TEST', 0, 1);";
		$this->CI->db->query($query); // ubacimo test korisnika
		$id = $this->CI->db->insert_id(); // dohvatimo ID
		
		$this->CI->useractions->promote_user($id); // odradimo promovisanje
		
		$query = "SELECT TKID FROM Korisnici WHERE KID = ?;";
		$res = $this->CI->db->query($query, array($id)); // dohvatimo promovisanog korisnika
		if ($res->num_rows() <= 0) {
			$this->fail("Nije uspeo upis u bazu"); // failujemo test
		}
		$row = $res->row();
		$query = "DELETE FROM Korisnici WHERE KID = ?;";
		$this->CI->db->query($query, array($id)); // obrisemo test korisnika
		
		$this->assertEquals(2, $row->TKID, "Korisnik nije promovisan u trenera"); // proverimo da li je tip korisnika 2 - Trener
	}

}
?>