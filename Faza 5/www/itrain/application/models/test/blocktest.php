<!-- autor: Stefan Ranković, 2014/3155 -->

<?php

include_once('/../mappers/blockmapper.php');
include_once('/../entity/block.php');
include_once('/../mappers/trainingmapper.php');
include_once('/../entity/training.php');


class BlockTest extends CI_Model {
	public function __construct() {
        parent::__construct();
    }
	
	public function test() {
		$dbm = new BlockMapper();
		$TID = 1;
		$block = new Block();
		$block->setBID(324);
		$block->setTitle("najnoviji blok");
		$block->setDescription("kao neki opis");
		$block->setBlockNum(3);
		$block->setDuration(22);
		$block->setTrainingID(1);
		//$dbm->store($block);
		$found = $dbm->find(1);
		foreach ($found as $block) {
			//echo $block;
		}
		//echo "Ukupno blokova za $TID:".$dbm->countBlocksFor($TID);
		//$dbm->delete(3);
		$tmp = new TrainingMapper();
		$tr = $tmp->find(9);
		//echo $tr[0];
		$trening = new Training();
		$trening->setTitle("NASLOV");
		$trening->setDescription("OPIS");
		$trening->setCreated(date("Y-m-d H:i:s"));
		$trening->setType(2);
		$trening->setOwnerID(3);
		$block1 = new Block();
		$block1->setTitle("BLOK1");
		$block1->setDescription("Prvi blok novog treninga");
		$block1->setBlockNum(1);
		$block1->setDuration(10);
		$block2 = new Block();
		$block2->setTitle("BLOK2");
		$block2->setDescription("Drugi blok novog treninga");
		$block2->setBlockNum(2);
		$block2->setDuration(20);
		$trening->setBlocks(array($block1, $block2));
		$tmp->store($trening);
		$trening->setTitle("NOVI NASLOV");
		$block3 = new Block();
		$block3->setTitle("BLOK3");
		$block3->setDescription("Treci blok");
		$block3->setBlockNum(3);
		$block3->setDuration(10);
		$trening->setBlocks(array($block1, $block2, $block3));
		echo $trening;
		$tmp->update($trening);
	}
}
?>
