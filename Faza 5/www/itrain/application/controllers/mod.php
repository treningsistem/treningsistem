<!-- autor: Milica Stanković 2009/0459 -->
<?php

include_once('basestaff.php');
include_once('/../models/entity/usertypes.php');

class Mod extends BaseStaff {
	public function __construct() { // kontroler koji nudi interface za korisnike
		parent::__construct();
		$userType = $this->session->userdata('type'); // dohvatimo tip logovanog korisnika
		if (!isset($userType) || $userType != UserTypes::Moderator) {
			redirect('start/staff', 'refresh'); // trener nije ulogovan, vracamo ga na login
			die();
		}
	}
}

?>
