<!-- autor: Milica Stanković 2009/0459 -->

<?php
class Editor extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('useractions');
	}

	public function index() {
		$this->load->view('hometemplate', array('body' => 'editor/main', 'title'=>'EDITOR | PSI projekat'));
	}

    public function trening($trening) {
        $this->load->view('editor/trening', array('trening' => $trening));
    }

    public function svi(){// pregled svih mojih treninga
        $trainings = $this->useractions->getMyTrainings($this->session->userdata('KID')); // ucitamo treninge


        $this->load->view('header', array('title'=>'Moji Treninzi | PSI projekat'));
        $this->load->view('menu/trenerMenu');
        $this->load->view('editor/main', array('trainings' => $trainings));
        $this->load->view('footer');

    }

    public function test(){

    }
}
?>
