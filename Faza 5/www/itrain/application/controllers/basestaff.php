<!-- autor: Milica Stanković 2009/0459 -->

<?php
include_once('/../models/entity/usertypes.php');
include_once('/../models/entity/user.php');
include_once('/../models/entity/trainingtypes.php');
include_once('/../models/entity/training.php');
include_once('/../models/entity/block.php');

abstract class BaseStaff extends CI_Controller {
// ova klasa predstavlja neke zajednicke funkcionalnosti administratora i moderatora
	public function __construct() {
	// kontoleri-klase Administrator i Moderator ce extendovati ovu klasu i implementirati specificne dodatne funkcionalnosti
		parent::__construct();
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('useractions');
		$this->load->model('mappers/trainingmapper');
        $this->load->model('mappers/usermapper');
		$this->load->model('mappers/blockmapper');
		$this->load->helper('pagination_style');
	}
	
	public function index() { // Homepage za korisnike i trenere
		$this->load->view('sharedstaff/stafftemplate', array('body' => 'sharedstaff/index', 'title' => 'Pocetna Strana'));
	}

    public function treninzi($page=0) { // Homepage za korisnike i trenere
		$perpage = 10; // 10 treninga po stranici
		$trainings= $this->useractions->getSomeTrainings((int)$page, $perpage); // ovo je bolje da stoji u kontroleru
		$config['base_url'] = site_url() . '/' . $this->session->userdata('typestring') . '/treninzi'; // formiramo linkove
		$config['total_rows'] = $this->useractions->countAllTrainings();
		$config['per_page'] = $perpage;
		$config = $config + page_style($config); // stilizujemo

		$this->pagination->initialize($config);
        $this->load->view('sharedstaff/stafftemplate', array('body' => 'sharedstaff/treninzi', 'title' => 'Treninzi', 'trainings' => $trainings));
    }

    public function korisnici($page=0) { // Homepage za korisnike i trenere
		$perpage = 10; // 10 treninga po stranici
		$users= $this->useractions->getSomeUsers((int)$page, $perpage);
		$config['base_url'] = site_url() . '/' . $this->session->userdata('typestring') . '/korisnici'; // formiramo linkove
		$config['total_rows'] = $this->useractions->countAllUsers();
		$config['per_page'] = $perpage;
		$config = $config + page_style($config); // stilizujemo

		$this->pagination->initialize($config);
        $this->load->view('sharedstaff/stafftemplate', array('body' => 'sharedstaff/korisnici',
            'title' => 'Korisnici', 'users' => $users));
    }

//////////////////////////////////////////////


    public function pregledaj($TID) { // pregled svih treninga
        $tr = $this->useractions->preview($TID); // dohvatamo trening
        if ($tr != null) {
            $this->load->view('sharedstaff/stafftemplate', array('body' => 'shareduser/preview',
                'title' => 'Trening ' . $tr->getTitle(), 'training' => $tr)); // ucitamo template
        } else {
            redirect($this->session->userdata('typestring') . '/treninzi', 'refresh');
            // pristup treningu nije dozvoljen, redirect na "treninzi"
        }
    }

    public function blokiraj($UID){
        // blokiraj korisnika sa $UID
        $this->useractions->block_user($UID); // briše se trening sa $TID

        redirect($this->session->userdata('typestring') . '/korisnici', 'refresh');
    }

    public function odblokiraj($UID){
        // blokiraj korisnika sa $UID
        $this->useractions->unblock_user($UID); // briše se trening sa $TID

        redirect($this->session->userdata('typestring') . '/korisnici', 'refresh');
    }


    public function unapredi($UID){
        // promeni status RK na Trener za korisnika sa $UID
        $this->useractions->promote_user($UID); // briše se trening sa $TID

        redirect($this->session->userdata('typestring') . '/korisnici', 'refresh');
        //pogled na administraciju svih korisnika
    }

    public function obrisi_trening($TID){

        $this->trainingmapper->delete($TID); // briše se trening sa $TID

        redirect($this->session->userdata('typestring') . '/treninzi', 'refresh');
        //pogled na administraciju svih treninga
    }
	
}
?>
