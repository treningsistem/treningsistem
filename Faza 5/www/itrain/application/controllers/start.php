<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
class Start extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('useractions');
	}

	public function index() {
		$this->load->view('hometemplate', array('body' => 'home/login', 'title'=>'START'));
	}

    public function staff() {
        $this->load->view('sharedstaff/staffindex', array('body' => 'sharedstaff/login', 'title'=>'STAFF'));
    }
	
	public function kontakt() {
		$this->load->view('sharedstaff/staffindex', array('body' => 'sharedstaff/kontakt', 'title'=>'CONTACT'));
	}

    public function registracija() {
		$this->load->view('hometemplate', array('body' => 'home/register', 'title'=>'REGISTRACIJA'));
    }

    public function about(){
		$this->load->view('hometemplate', array('body' => 'home/about', 'title'=>'ABOUT'));
    }
}
?>
