Kada se pokrene WAMP u system tray ima ikonicu, levi klik i izabere se 'phpMyAdmin', onda se u Create Database polje upise itrain i klikne 'Create'. U tabeli ce se pojaviti baza itrain, kliknite na nju. Onda idete na import i izaberete neki od sql fajlova koji hocete da izvrsite.

U folderu sql nalaze se 3 skripte:
'dbsetup.sql' pravi tabele u bazi itrain
'dbclear.sql' brise sve tabele iz baze
'dbtest.sql'  puni tabele napravljene sa dbsetup.sql sa test vrednostima

Folder sql nije deo stabla web servera, znaci nemojte da ga pakujete u WAMP nego negde drugde.

Folder www je kopija mog foldera www iz WAMP. Tu se nalazi instaliran codeigniter u folderu itrain. Ovde dodajemo php fajlove. Ako hocete da pokrenete projekat, otvorite browser i ucukate 'localhost/itrain' bez navodnika.