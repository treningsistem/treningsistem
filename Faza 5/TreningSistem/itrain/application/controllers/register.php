<!-- autor: Milica Stanković 2009/0459 -->

<?php
include_once('/../models/entity/user.php');
include_once('/../models/entity/usertypes.php');

class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('regmodel');
        $this->load->library('form_validation');
        $this->load->helper('form');

    }

    public function validation()
    {
        $username = $this->input->post('username'); // pokupimo parametre
        $password1 = $this->input->post('password1');
        $password2 = $this->input->post('password2');
        $email = $this->input->post('email');

        $this->form_validation->set_rules('username', 'Korisničko ime', 'required',
            array('required' => '%s je obavezno.')
        );
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email',
            array('required' => '%s je obavezan.',
                    'valid_email' => '%s nije validna adresa.')
        );
        $this->form_validation->set_rules('password1', 'Lozinka', 'required|min_length[5]',
            array('required' => '%s je obavezna.',
                    'min_length' => '%s mora imati bar 5 karaktera.')
        );
        $this->form_validation->set_rules('password2', 'Ponovljena lozinka',
            'required|matches[password1]',
            array('required' => '%s je obavezna.',
                    'matches' => 'Lozinka se ne slaže.'
            )
            // TODO: matches za password ne ispisuje moju poruku nego generičku - zašto?
        );

        if ($this->form_validation->run() == true) {// odradimo validaciju
            $user = new User(); // napravimo novi korisnicki objekat
            $user->setUsername($username);
            $user->setPassword($password1);
            $user->setEmail($email);
            $user->setBlocked(0);
            $user->setType(UserTypes::RK);
            $this->regmodel->register($user); // ako prodje validacija registrujemo

            $this->load->view('hometemplate', array('body' => 'home/regsucc', 'title' => 'USPEŠNA REGISTRACIJA'));
        } else {
            $this->load->view('hometemplate', array('body' => 'home/register', 'title' => 'NEUSPEŠNA REGISTRACIJA'));
        }
    }
}
?>
