<!-- autor: Milica Stanković 2009/0459 -->

<?php
/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 20.5.2015.
 * Time: 22:59
 */


class Tekuce extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('test/mtest');
    }

    public function index() {
        // TODO: Milica - testiranje za view-ove

        $this->load->view('header', array('title'=>'TEST | PSI projekat'));
        $this->load->view('admin/adminSviTreninzi');
        $this->load->view('footer');
    }

    function adminsvitreninzi(){
        $treninzi = $this->TrainingMapper->findAllPublic(); //array of members
        $this->load->view('members.php', $treninzi); // load members in view
    }

}
?>
