<!-- autor: Stefan Ranković, 2014/3155 -->

<?php
include_once('/../models/entity/trainingtypes.php');
include_once('/../models/entity/training.php');
include_once('/../models/entity/block.php');

abstract class BaseUser extends CI_Controller {	// ova klasa predstavlja neke zajednicke funkcionalnosti trenera i registrovanih korisnika
	public function __construct() {     // kontoleri-klase Korisnik i Trener ce extendovati ovu klasu i implementirati specificne dodatne funkcionalnosti 
		parent::__construct();
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->helper('url');
		$this->load->model('useractions');
		$this->load->model('mappers/trainingmapper');
		$this->load->model('mappers/blockmapper');
		$this->load->helper('pagination_style');
		$this->useractions->loadMyTrainings($this->session->userdata('KID')); // ucitava sve treninge koji pripadaju ovom korisniku
	}
	
	public function index() { // Homepage za korisnike i trenere
		$this->load->view('usertemplate', array('body' => 'shareduser/index', 'title' => 'Home'));
	}
	
	public function moji($page=0) { // pregled svih mojih treninga
        $perpage = 10; // neka bude hardcoded na 10, dok ne smislim nesto bolje
		$trainings = $this->useractions->getMyTrainings(); // ucitamo treninge
		$config['base_url'] = 'http://localhost/itrain/index.php/' . $this->session->userdata('typestring') . '/moji'; // formiramo linkove
		$config['total_rows'] = count($trainings);
		$config['per_page'] = $perpage;
		$config = $config + page_style($config); // stilizujemo

		$this->pagination->initialize($config);
		$trainings = array_slice($trainings, $page, $perpage); // uzmemo onoliko treninga koliko nam treba
		$this->load->view('usertemplate', array('body' => 'shareduser/moji', 'title' => 'Moji Treninzi', 'trainings' => $trainings));
    }

	public function javni($page=0) { // pregled svih javnih treninga
		$perpage = 10; // neka bude hardcoded na 10, dok ne smislim nesto bolje
		$trainings = $this->useractions->getSomePublicTrainings((int)$page, $perpage);
		$config['base_url'] = 'http://localhost/itrain/index.php/' . $this->session->userdata('typestring') . '/javni';
		$config['total_rows'] = $this->useractions->countPublicTrainings();
		$config['per_page'] = $perpage;
		$config = $config + page_style($config); // stilizujemo
		
		$this->pagination->initialize($config);
		$this->load->view('usertemplate', array('body' => 'shareduser/javni', 'title' => 'Javni Treninzi', 'trainings' => $trainings));
	}
	
	public function novi() { // treneri mogu da biraju tip, korisnici ne, trebace 2 slicne stranice
		$this->useractions->newTraining(); // kreira novi trening
		$this->load->view('usertemplate', array('body' => 'editor/main', 'title' => 'Editor',
            'training' => $this->session->userdata('currentTraining')));
	    // view za novi trening
    }
	
	public function kreiraj() { // kada se popuni view iz funkcije novi() sa informacijama, radi se kreiranje
		$title = $this->input->post('title');  // naslov
		$description = $this->input->post('description'); // opis
		$type = $this->input->post('type'); // javni ili privatni, bitno samo ako je korisnik trener
		if (empty($title) || empty($description) || empty($type)) { // ovaj type moze biti hidden polje setovano na 1 za obicne korisnike
			// error view za ovo
            $this->load->view('errors/html/error', array(
                'title' => 'Popuniti sva polja',
                'heading' => 'Greška: neka polja su ostala prazna',
                'message' => 'Poštovani,<br />Sva polja forme moraju biti popunjena, probajte ponovo.'
            ));
			return;
		}
		if ($this->session->userdata('type') == UserTypes::RK) { // za slucaj da neko izmeni hidden polje
			$type = TrainingTypes::PrivateTraining;
		}
		$this->useractions->createTraining($title, $description, $type);
		redirect($this->session->userdata('typestring') . '/moji', 'refresh');
	}
	
	public function obrisi($TID) { // brise trening sa $TID
		if ($this->useractions->isMyTraining($TID)) { // provera da li su podaci ispravni
			$this->trainingmapper->delete($TID); // ako trening pripada korisniku, brise se
		}
		redirect($this->session->userdata('typestring') . '/moji', 'refresh');
	}
	
	public function izmeni($TID) { // menja trening $TID. Izmena podrazumeva: dodavanje blokova, brisanje blokova, promenu opisa i naslova treninga
		if ($this->useractions->isMyTraining($TID)) { // provera da li su podaci ispravni
			$this->useractions->loadTraining($TID); // ucitamo trening
            $this->load->view('usertemplate', array('body' => 'editor/main', 'title' => 'Editor', 'training' => $this->session->userdata('currentTraining')));
		} else {
			redirect($this->session->userdata('typestring') . '/moji', 'refresh'); // redirekt na moje treninge
		}
	}
	
	public function pregledaj($TID) { // pregled treninga
		$tr = $this->useractions->preview($TID); // dohvatamo trening
		if ($tr != null) {
			$this->load->view('usertemplate', array('body' => 'shareduser/preview', 'title' => 'Trening ' . $tr->getTitle(), 'training' => $tr)); // ucitamo template
		} else {
			redirect($this->session->userdata('typestring') . '/moji', 'refresh'); // pristup treningu nije dozvoljen, redirect na "moji"
		}
	}
	
	public function obrisi_blok() {
		$TID = $this->input->post('TID'); // uzmemo TID
		if ($this->useractions->isMyTraining($TID)) { // provera da li su podaci ispravni
			$blockNum = $this->input->post('blockNum'); // redni broj bloka koji se brise
			$this->useractions->removeBlock($TID, $blockNum); // brise blok sa rednim brojem $blockNum iz $TID
		}
		// TODO : ponovo ucitati view za izmenu treninga $TID
	}
	
	public function dodaj_blok() {
		$TID = $this->input->post('TID');
		if ($this->useractions->isMyTraining($TID)) { // provera da li su podaci ispravni
			$title = $this->input->post('title');  // prikupimo informacije o bloku
			$description = $this->input->post('description');
			$blockNum = $this->input->post('blockNum');
			$duration = $this->input->post('duration');
			$this->useractions->createBlock($TID, $title, $description, $blockNum, $duration); // pravi blok, dodaje ga u trening i snima u bazu
		}
		// TODO : ponovo ucitati view za izmenu treninga $TID
	}
	
	public function snimi() { // cuva sve izmene obavljene nad treningom
		$currentTraining = $this->session->userdata('currentTraining');
		$currentTraining->setBlocks(array()); // obrisemo postojece blokove
		if (!empty($this->input->post('trnaslov'))) { // provera na naslov treninga (trening objekat ima default vrednosti)
			$currentTraining->setTitle($this->input->post('trnaslov'));
		}
		if (!empty($this->input->post('tropis'))) { // provera da li je popunjen opis (trening objekat ima default vrednosti)
			$currentTraining->setDescription($this->input->post('tropis'));
		}
		// posto je komplikovan uslov za odredjivanje tipa, razbicu ga na delove
		$trcond = !empty($this->input->post('trtip')); // prvo proverimo da polje nije empty
		$trcond = $trcond && $this->session->userdata('type') == UserTypes::Trener; // onda proverimo da li je korisnik zapravo trener
		$trcond = $trcond && ($this->input->post('trtip') == TrainingTypes::PublicTraining || $this->input->post('trtip') == TrainingTypes::PrivateTraining); // onda proverimo da li je tip zapravo javni ili privatni
		if ($trcond) { // podrazumevano privatni, ako gornji uslov ne prodje
			$currentTraining->setType($this->input->post('trtip'));
		}
		$i = 0;
		while (true) {
			$naziv = $this->input->post('naziv' . $i); // naziv bloka
			$opis = $this->input->post('opis' . $i); // naziv bloka
			$trajanje = $this->input->post('trajanje' . $i); // naziv bloka
			if (empty($naziv) && empty($opis) && empty($trajanje)) {
				break; // iskacemo iz petlje ako je sve empty
			}
			$b = new Block(); // napravimo novi blok
			$b->setTitle($naziv);
			$b->setDescription($opis);
			$b->setBlockNum($i);
			$b->setDuration($trajanje);
			$currentTraining->append($b);
			$i++; // prelazimo na sledeci blok
		}
		$currentTraining->setOwnerID($this->session->userdata('KID')); // postavimo vlasnika
		$this->useractions->save($currentTraining); // model cuva trening u bazi
		redirect($this->session->userdata('typestring') . '/moji');
	}
	
}
?>
