<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->



<?php
$this->load->view('header', array('title'=>'iTrain | ' . $title)); // header, treba prolsediti naslov
$this->load->view('menu/globalMenu'); // globalni meni, izvucen iz headera
?>

<!-- start: Content-->
<div id="content" name="myContent" title="Sadrzaj" float="right" >

<?php
$this->load->view($body); // treba proslediti i putanju do tela stranice
?>

<!-- end: Content-->
	<div class="push"></div>
</div>

<?php
$this->load->view('footer');
?>
