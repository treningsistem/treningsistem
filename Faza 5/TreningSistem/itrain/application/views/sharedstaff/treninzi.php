<!-- autor: Milica Stanković 2009/0459 -->
<!-- autor: Stefan Ranković, 2014/3155 -->


<?php

$this->load->library('table'); // pomoc kod kreiranja tabele
$this->load->library('session');
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$CI->table->set_heading('Naslov', 'Autor', 'Datum', 'Opis', 'Briši'); // postavimo heading
$usertype = $CI->session->userdata('typestring');

foreach ($trainings as $tr) { // PAŽNJA: radi sa redovima iz baze

    $CI->table->add_row(
        (($tr->TTID)==1) ?
            anchor($CI->session->userdata('typestring') . '/pregledaj/' . $tr->TID, $tr->Title) :
            $tr->Title,
        $tr->Username,
        $tr->Created,
        strlen($tr->Description) > 30 ? substr($tr->Description, 0, 30) . "[...]" : $tr->Description,
        (($tr->TTID)==1) ? "Javni" : "Privatni",
        anchor($usertype . '/obrisi_trening/' . $tr->TID, 'X')
    );

}

echo $CI->table->generate(); // napravimo tabelu
echo $CI->pagination->create_links(); // napravimo linkove za paginaciju

/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 21.5.2015.
 * Time: 1:33
 */

?>

</br><div class=system>1) nije dozvoljen pogled na privatne treninge.</div>
<div class=system>2) brisanje treninga je trajna izmena u bazi.</div>
