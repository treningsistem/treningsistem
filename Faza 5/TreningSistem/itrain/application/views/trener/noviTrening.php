<!--
/**
 * Created by PhpStorm.
 * User: Milica
 * Date: 19.5.2015.
 * Time: 1:41
 */ -->

<?php
$this->load->helper('form_helper'); // treba nam form helper
$this->load->library('table'); // pomoc kod kreiranja tabele

echo form_open("trener/kreiraj", array("method"=>"post")); // otvorimo formu, tip je post
$CI =& get_instance(); // dohvatimo instancu codeignitera (jer sledece linije ne rade preko $this)
$CI->table->add_row(form_label("Naziv treninga:", ""), form_input('title', ''));
$CI->table->add_row(form_label("Opis:", ""), form_textarea('description', ''));
$CI->table->add_row(form_label("Deljenje?", ""));
$CI->table->add_row(form_radio('type', '1', FALSE), form_label("Javni", ""));
$CI->table->add_row(form_radio('type', '2', TRUE), form_label("Privatni", ""));

$CI->table->add_row( form_submit('', 'Napravi')); // submit dugme

echo $CI->table->generate(); // napravimo tabelu


echo form_close(); // zatvorimo formu

/* TODO: ovde uputiti na odgovarajuću akciju za kreiranje novog treninga!*/

?>