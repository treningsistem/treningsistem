Instrukcije za instalaciju i pokretanje testova na phpunit i selenium platformama.

PHPUNIT:
Ovde je fajl phpunit.phar, php arhiva sa potrebnim klasama (ukljucujuci i klase za selenium, ako cemo uopste koristiti selenium). Potrebno je instalirati prema ovom sablonu, sa sajta https://phpunit.de/manual/current/en/installation.html#installation.phar.windows

1. Create a directory for PHP binaries; e.g., C:\bin
2. Append ;C:\bin to your PATH environment variable (related help)
3. Download https://phar.phpunit.de/phpunit.phar and save the file as C:\bin\phpunit.phar
4. Open a command line (e.g., press Windows+R » type cmd » ENTER)
5. Create a wrapping batch script (results in C:\bin\phpunit.cmd):
	C:\Users\username> cd C:\bin
	C:\bin> echo @php "%~dp0phpunit.phar" %* > phpunit.cmd
	C:\bin> exit
	
Ovo u principu znaci:
1. Napraviti folder C:\bin
2. Za windows 7: Kliknuti Win Key (izmedju CTRL i ALT) i Pause -> Advanced System Settings -> Environment Variables -> User Variables for [User], kliknuti na Edit... -> na kraju stringa dodati ";C:\bin", bez navodnika, slepiti sa ostatkom stringa
3. Skinuti .phar - vec sam ga prosledio u folderu
4. Otvoriti cmd prompt
5. Ukucati ove 3 komande

Posle toga je phpunit spreman za rad.

SELENIUM:
ODAVDE SKINUTI SELENIUM SERVER (.jar fajl, 35 MB): selenium-release.storage.googleapis.com/2.45/selenium-server-standalone-2.45.0.jar
Selenium je bitan samo ako se koriste selenium klase, ako ga ne budemo koristili, neka ipak bude tu (Posto vidim da se pominje testiranje formi, selenium i sluzi za to, tako da ga najverovatnije moramo koristiti). Prosledio sam batch skriptu za dizanje selenium servera (.bat). Pre pokretanja testova koji koriste selenium klase, mora se preko skripte podici server (obican dvoklik na .bat). Server se gasi sa CTRL+C i kada pita are you sure -> 'Y' + ENTER. Moze i na X.
NEMOJTE DA POKRECETE .jar DIREKTNO JER NE PRAVI INTERFEJS.
Ako ipak pokrenete .jar i hocete da ga ugasite, CTRL + SHIFT + ESC -> naci 'javaw.exe' i ugasiti taj proces.

TESTOVI:
Testove stavljati u folder "testovi" i MORAJU IM SE IMENA ZAVRSAVATI sa "Test.php". Za pokretanje testova:
1. Otvoriti cmd prompt
2. Pozicionirati se u folder sa testovima (komanda "cd bla/bla/bla/Faza8")
3. Testovi se pojedinacno pokrecu komandom "phpunit testovi/nekitestTest.php".
4. Mogu se pokrenuti i svi testovi u folderu za komandom "phpunit testovi".
